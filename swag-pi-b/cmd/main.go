package main

import (
	"log"
	"math"
	"net/http"
	"strconv"

	_ "swag-pi-b/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title Simple API Example
// @version 1.0
// @description This is a simple API example built with Go and Gin.
// @termsOfService https://example.com/terms
// @contact.name API Support
// @contact.url https://www.example.com/support
// @license.name MIT
// @license.url https://opensource.org/licenses/MIT
func main() {
	r := gin.Default()

	// Routes
	r.GET("/health", HealthCheck)

	// API group v1
	v1 := r.Group("/api/v1")
	{
		v1.GET("/swag-pi-b", CalculatePhiValue)
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// kick in yoo!
	log.Println("Starting server on :8080...")
	log.Fatal(http.ListenAndServe(":8080", r))
}

// CalculatePhiValue calculates the Phi value of a number.
// @Summary Calculates the Phi value of a number.
// @Description This endpoint calculates the Phi value of a number.
// @ID calculate-phi-value
// @Produce json
// @Param number query int true "Number to calculate Phi value for"
// @Success 200 {string} string "Phi value"
// @Router /api/v1/swag-pi-b [get]
func CalculatePhiValue(c *gin.Context) {
	numberStr := c.Query("number")
	number, err := strconv.Atoi(numberStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid number"})
		return
	}

	phi := (1 + math.Sqrt(5)) / 2
	phiValue := phi * float64(number)
	c.JSON(http.StatusOK, gin.H{"phi_value": phiValue})
}

// HealthCheck returns a health check response.
// @Summary Returns the health check response.
// @Description This endpoint returns a health check response.
// @ID health-check
// @Produce json
// @Success 200 {string} string "OK"
// @Router /health [get]
func HealthCheck(c *gin.Context) {
	c.String(http.StatusOK, "Iam Okay!")
}
