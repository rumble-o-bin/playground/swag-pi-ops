CLUSTER_NAME="kind-infras"
CLUSTER_VERSION="kindest/node:v1.25.11"

.PHONY: help
help:  ## Show help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

start_cluster: ## Start kind cluster
	@kind create cluster --name $(CLUSTER_NAME) --config infra/config.yaml --image $(CLUSTER_VERSION)

delete_cluster: ## Delete cluster kind
	@kind delete cluster --name $(CLUSTER_NAME)

install_kong: ## Install kong
	@kubectl apply -k kong/pg-kong
	@sleep 12
	@helm upgrade --install -n kong kangkong kong/ -f kong/values.yaml -f kong/values-env.yaml
