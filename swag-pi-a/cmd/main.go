package main

import (
	"log"
	"math/rand"
	"net/http"

	_ "swag-pi-a/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title Simple API Example
// @version 1.0
// @description This is a simple API example built with Go and Gin.
// @termsOfService https://example.com/terms
// @contact.name API Support
// @contact.url https://www.example.com/support
// @license.name MIT
// @license.url https://opensource.org/licenses/MIT
func main() {
	r := gin.Default()

	// Routes
	r.GET("/health", HealthCheck)

	// API group v1
	v1 := r.Group("/api/v1")
	{
		v1.GET("/swag-pi-a", GetRandomQuote)
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// kick in yoo!
	log.Println("Starting server on :8080...")
	log.Fatal(http.ListenAndServe(":8080", r))
}

// GetRandomQuote returns a random quote.
// @Summary Returns a random quote.
// @Description This endpoint returns a random quote.
// @ID get-random-quote
// @Produce json
// @Success 200 {string} string "Random quote"
// @Router /api/v1/swag-pi-a [get]
func GetRandomQuote(c *gin.Context) {
	// https://www.goodreads.com/quotes/tag/random :lol:
	quotes := []string{
		"You are the shuckiest shuck faced shuck in the world!",
		"Her name badge read: Hello! My name is DIE, DEMIGOD SCUM!",
		"Insane means fewer cameras!",
	}

	randomIndex := rand.Intn(len(quotes))
	c.JSON(http.StatusOK, gin.H{"quote": quotes[randomIndex]})
}

// HealthCheck returns a health check response.
// @Summary Returns the health check response.
// @Description This endpoint returns a health check response.
// @ID health-check
// @Produce json
// @Success 200 {string} string "OK"
// @Router /health [get]
func HealthCheck(c *gin.Context) {
	c.String(http.StatusOK, "Iam Okay!")
}
